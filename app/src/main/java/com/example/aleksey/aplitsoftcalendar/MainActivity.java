package com.example.aleksey.aplitsoftcalendar;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.provider.CalendarContract.*;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.text.format.DateFormat;
import android.widget.Toast;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    public static final String[] EVENT_PROJECTION = new String[] {
            Calendars._ID,                           // 0
            Calendars.ACCOUNT_NAME,                  // 1
            Calendars.CALENDAR_DISPLAY_NAME,         // 2
            Calendars.OWNER_ACCOUNT                  // 3
    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
    private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;

    Button b_dateStart;
    Button b_dateEnd;
    Button b_save;
    Button b_findByStart;
    Button b_findByEnd;
    Button b_findByTitle;
    Button b_findByLocation;
    Button b_update;
    Button b_delete;
    TextView t_dateStart;
    TextView t_dateEnd;
    EditText e_title;
    EditText e_location;
    EditText e_description;

    int dayStart, monthStart, yearStart, hourStart, minuteStart;
    int dayEnd, monthEnd, yearEnd, hourEnd, minuteEnd;

    long eventID = -1;
    //int eventNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Check and add permissions
        checkPermissions(this);

        b_dateStart = (Button) findViewById(R.id.b_dateStart);
        b_dateEnd = (Button) findViewById(R.id.b_dateEnd);
        t_dateStart = (TextView) findViewById(R.id.t_dateStart);
        t_dateEnd = (TextView) findViewById(R.id.t_dateEnd);
        b_save = (Button) findViewById(R.id.b_save);
        b_findByStart = (Button) findViewById(R.id.b_findByStart);
        b_findByEnd = (Button) findViewById(R.id.b_findByEnd);
        b_findByTitle = (Button) findViewById(R.id.b_findByTitle);
        b_findByLocation = (Button) findViewById(R.id.b_findByLocation);
        b_update = (Button) findViewById(R.id.b_update);
        b_delete = (Button) findViewById(R.id.b_delete);
        e_title = (EditText) findViewById(R.id.e_title);
        e_location = (EditText) findViewById(R.id.e_location);
        e_description = (EditText) findViewById(R.id.e_description);

        //Start value for variables
        final Calendar calendar = Calendar.getInstance();
        yearStart = yearEnd = calendar.get(Calendar.YEAR);
        monthStart = monthEnd = calendar.get(Calendar.MONTH);
        dayStart = dayEnd = calendar.get(Calendar.DAY_OF_MONTH);
        hourStart = hourEnd = calendar.get(Calendar.HOUR_OF_DAY);
        minuteStart = minuteEnd = calendar.get(Calendar.MINUTE);

        b_dateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t_dateStart.setText("");

                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, MainActivity.this,
                        yearStart, monthStart, dayStart);
                datePickerDialog.show();
            }
        });

        b_dateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t_dateEnd.setText("");

                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, MainActivity.this,
                        yearEnd, monthEnd, dayEnd);
                datePickerDialog.show();
            }
        });

        b_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventID = -1;
                String title = e_title.getText().toString();
                String location = e_location.getText().toString();
                String description = e_description.getText().toString();

                long calendarID = getCalendarId();
                eventID = addEvent(calendarID, title, location, description);

                Toast msg = eventID > -1
                        ? Toast.makeText(getApplicationContext(), "Event added", Toast.LENGTH_SHORT)
                        : Toast.makeText(getApplicationContext(), "Error! Event didn't add", Toast.LENGTH_SHORT);
                msg.show();
            }
        });

        b_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast msg;

                if (eventID > -1) {
                    deleteEvent(eventID);
                    msg = Toast.makeText(getApplicationContext(), "Event deleted", Toast.LENGTH_SHORT);
                } else
                    msg = Toast.makeText(getApplicationContext(), "Event didn't delete", Toast.LENGTH_SHORT);

                msg.show();
            }
        });

        b_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast msg;
                //System.out.println("eventID: " + eventID);

                if (eventID > -1) {
                    String title = e_title.getText().toString();
                    String location = e_location.getText().toString();
                    String description = e_description.getText().toString();
                    //System.out.println(title + " " + location + " " + description);

                    updateEvent(eventID, title, location, description);
                    msg = Toast.makeText(getApplicationContext(), "Event updated", Toast.LENGTH_SHORT);
                } else
                    msg = Toast.makeText(getApplicationContext(), "Event didn't update", Toast.LENGTH_SHORT);

                msg.show();
            }
        });

        b_findByStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendarDTstart = Calendar.getInstance();
                calendarDTstart.set(yearStart, monthStart, dayStart, hourStart, minuteStart, 0);
                String searchQuery = "DTSTART / 1000 = " + calendarDTstart.getTimeInMillis() / 1000;
                //System.out.println(searchQuery);
                viewEvent(searchQuery);
            }
        });

        b_findByEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendarDTend = Calendar.getInstance();
                calendarDTend.set(yearEnd, monthEnd, dayEnd, hourEnd, minuteEnd, 0);
                String searchQuery = "DTEND / 1000 = " + calendarDTend.getTimeInMillis() / 1000;
                //System.out.println(searchQuery);
                viewEvent(searchQuery);
            }
        });

        b_findByTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchQuery = "TITLE = '" + e_title.getText() +"'";
                //System.out.println(searchQuery);
                viewEvent(searchQuery);
            }
        });

        b_findByLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchQuery = "EVENTLOCATION = '" + e_location.getText() + "'";
                //System.out.println(searchQuery);
                viewEvent(searchQuery);
            }
        });
    }

    private boolean checkPermissions(Activity activity) {
        String[] permissions = new String[] {Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR};
        int needPermissions = 0;
        boolean result = true;

        for (String permission : permissions) {
            int checkPermission = ActivityCompat.checkSelfPermission(activity, permission);
            if (checkPermission != PackageManager.PERMISSION_GRANTED)
                needPermissions++;
        }

        if (needPermissions > 0) {
            ActivityCompat.requestPermissions(activity, permissions, 0);
            result = false;
        }

        //ActivityCompat.OnRequestPermissionsResultCallback(0, permissions, );

        return result;
    }

    private long getCalendarId() {
        Cursor cur = null;
        ContentResolver cr = getContentResolver();
        Uri uri = Calendars.CONTENT_URI;
        String selection = "((" + Calendars.ACCOUNT_NAME + " like ?) AND ("
                + Calendars.ACCOUNT_TYPE + " = ?) AND ("
                + Calendars.OWNER_ACCOUNT + " like ?))";
        //String selection = "(" + Calendars.ACCOUNT_TYPE + " = ?)";
        String[] selectionArgs = new String[]{"%", "com.google", "%"};
        // Submit the query and get a Cursor object back.

        cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);


        //while (cur.moveToNext()) {
        cur.moveToNext();
        long calID = 0;
        //String displayName = null;
        //String accountName = null;
        //String ownerName = null;

        // Get the field values
        calID = cur.getLong(PROJECTION_ID_INDEX);
        //displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
        //accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
        //ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);

        return calID;
    }

    private long addEvent(long calID, String title, String location, String description) {
        //long calID = 3;
        long startMillis = 0;
        long endMillis = 0;

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(yearStart, monthStart, dayStart, hourStart, minuteStart, 0);
        startMillis = beginTime.getTimeInMillis();

        Calendar endTime = Calendar.getInstance();
        endTime.set(yearEnd, monthEnd, dayEnd, hourEnd, minuteEnd, 0);
        endMillis = endTime.getTimeInMillis();


        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(Events.DTSTART, startMillis);
        values.put(Events.DTEND, endMillis);
        values.put(Events.TITLE, title);
        values.put(Events.DESCRIPTION, description);
        values.put(Events.CALENDAR_ID, calID);
        values.put(Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().toString());
        values.put(Events.EVENT_LOCATION, location);
        Uri uri = cr.insert(Events.CONTENT_URI, values);

// get the event ID that is the last element in the Uri
        long eventID = Long.parseLong(uri.getLastPathSegment());

        return eventID;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int hour = 0;
        int minute = 0;

        if (t_dateStart.getText().equals("")) {
            yearStart = year;
            monthStart = month;
            dayStart = dayOfMonth;
            hour = hourStart;
            minute = minuteStart;
        }

        if (t_dateEnd.getText().equals("")) {
            yearEnd = year;
            monthEnd = month;
            dayEnd = dayOfMonth;
            hour = hourEnd;
            minute = minuteEnd;
        }
        //Calendar calendar = Calendar.getInstance();
        //hour = calendar.get(Calendar.HOUR_OF_DAY);
        //minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, MainActivity.this,
                hour, minute, DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
        //hour = hourOfDay;
        //minute = minutes;

        if (t_dateStart.getText().equals("")) {
            hourStart = hourOfDay;
            minuteStart = minutes;
            Calendar calendar = Calendar.getInstance();
            calendar.set(yearStart, monthStart, dayStart, hourStart, minuteStart, 0);
            t_dateStart.setText(getDate(calendar.getTimeInMillis()));
            //t_dateStart.setText(dayStart + "/" + (monthStart + 1) + "/" + yearStart + " " + hourStart + ":" + minuteStart);
        }
        if (t_dateEnd.getText().equals("")) {
            hourEnd = hourOfDay;
            minuteEnd = minutes;
            Calendar calendar = Calendar.getInstance();
            calendar.set(yearEnd, monthEnd, dayEnd, hourEnd, minuteEnd, 0);
            t_dateEnd.setText(getDate(calendar.getTimeInMillis()));
            //t_dateEnd.setText(dayEnd + "/" + (monthEnd + 1) + "/" + yearEnd + " " + hourEnd + ":" + minuteEnd);
        }
    }

    public class eventInfo {
        public long eventID;
        public String eventTitle;
        public long dateStart;
        public long dateEnd;
        public String description;
        public String location;

        public eventInfo(long eventID, String eventTitle, long dateStart, long dateEnd, String description, String location) {
            this.eventID = eventID;
            this.eventTitle = eventTitle;
            this.dateStart = dateStart;
            this.dateEnd = dateEnd;
            this.description = description;
            this.location = location;
        }
    }

    ArrayList<eventInfo> events = new ArrayList();

    private void viewEvent(String searchQuery) {
        Cursor cursor = MainActivity.this.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[] { "_id", "title", "dtstart", "dtend", "description", "eventLocation" },
                        searchQuery,null, null);
        cursor.moveToFirst();
        // fetching calendars name
        String[] CNames = new String[cursor.getCount()];

        // fetching calendars id
        events.clear();
        for (int i = 0; i < CNames.length; i++) {
            events.add(new eventInfo(
                    cursor.getLong(0),
                    cursor.getString(1),
                    //getDate(Long.parseLong(cursor.getString(2))),
                    //getDate(Long.parseLong(cursor.getString(3))),
                    cursor.getLong(2),
                    cursor.getLong(3),
                    cursor.getString(4),
                    cursor.getString(5)
            ));
            //System.out.println(cursor.getString(0) + "   " + searchQuery);

            //nameOfEvent.add(cursor.getString(1));
            //startDates.add(getDate(Long.parseLong(cursor.getString(3))));
            //endDates.add(getDate(Long.parseLong(cursor.getString(4))));
            //descriptions.add(cursor.getString(2));
            CNames[i] = cursor.getString(1);
            cursor.moveToNext();

        }

//        for (eventInfo event : events) {
//            System.out.println(event.eventID + " " + event.eventTitle);
//        }

        if (events.size() == 1) {
            eventID = events.get(0).eventID;
            showEventOnForm(events.get(0));
        } else {
            String[] eventsString = new String[events.size()];
            int i = 0;
            for (eventInfo event : events) {
                //eventsString[i++] = event.dateStart + " - " + event.dateEnd + " " + event.eventName;
                eventsString[i++] = event.eventTitle;
            }

            AlertDialog.Builder selectionBuilder = new AlertDialog.Builder(MainActivity.this);

            selectionBuilder.setItems(eventsString, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //System.out.println("Old eventID: " + eventID);
                    eventID = events.get(which).eventID;
                    //System.out.println("New eventID: " + eventID);
                    showEventOnForm(events.get(which));
                    dialog.cancel();
                }
            });

            selectionBuilder.setTitle("Select event");
            AlertDialog selectionDialog = selectionBuilder.create();
            selectionDialog.show();

        }
    }

    private void showEventOnForm(eventInfo event) {
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.setTimeInMillis(event.dateStart);
        yearStart = calendarStart.get(Calendar.YEAR);
        monthStart = calendarStart.get(Calendar.MONTH);
        dayStart = calendarStart.get(Calendar.DAY_OF_MONTH);
        hourStart = calendarStart.get(Calendar.HOUR_OF_DAY);
        minuteStart = calendarStart.get(Calendar.MINUTE);

        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTimeInMillis(event.dateEnd);
        yearEnd = calendarEnd.get(Calendar.YEAR);
        monthEnd = calendarEnd.get(Calendar.MONTH);
        dayEnd = calendarEnd.get(Calendar.DAY_OF_MONTH);
        hourEnd = calendarEnd.get(Calendar.HOUR_OF_DAY);
        minuteEnd = calendarEnd.get(Calendar.MINUTE);

        e_title.setText(event.eventTitle);
        e_location.setText(event.location);
        e_description.setText(event.description);
        t_dateStart.setText(getDate(event.dateStart));
        t_dateEnd.setText(getDate(event.dateEnd));
    }

    private static String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private int deleteEvent(long eventID) {
        int result = 0;
        //ContentResolver cr = getContentResolver();
        //ContentValues values = new ContentValues();
        Uri deleteUri = null;
        deleteUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventID);
        result = getContentResolver().delete(deleteUri, null, null);

        return result;
    }

    private int updateEvent(long eventID, String title, String location, String description) {
        int result = 0;
        long startMillis = 0;
        long endMillis = 0;
        Uri updateUri = null;

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(yearStart, monthStart, dayStart, hourStart, minuteStart, 0);
        startMillis = beginTime.getTimeInMillis();

        Calendar endTime = Calendar.getInstance();
        endTime.set(yearEnd, monthEnd, dayEnd, hourEnd, minuteEnd, 0);
        endMillis = endTime.getTimeInMillis();

        //ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(Events.DTSTART, startMillis);
        values.put(Events.DTEND, endMillis);
        values.put(Events.TITLE, title);
        values.put(Events.DESCRIPTION, description);
        values.put(Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().toString());
        values.put(Events.EVENT_LOCATION, location);

        updateUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventID);
        result = getContentResolver().update(updateUri, values, null, null);

        return result;
    }
}
